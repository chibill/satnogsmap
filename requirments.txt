requests>=2.21.0
flask>=1.0.2
apscheduler>=3.5.0
tqdm>=4.30.0
skyfield>=1.41
numpy >=1.16
-e git+https://gitlab.com/librespacefoundation/satnogs/python-satnogs-api@e20a7d3c98526edb55f0dd20f58a4295f09a7bff#egg=satnogs_api_client
